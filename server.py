'''
Author: Maxime MOREILLON
'''

from flask import Flask, escape, request, jsonify
from flask_cors import CORS
from dotenv import load_dotenv
import os
import cv2
import numpy as np
import requests
import json

load_dotenv()

# Allowing to set size using environment variables
IMAGE_WIDTH = int(os.getenv("IMAGE_WIDTH")  or 320)
IMAGE_HEIGHT = int(os.getenv("IMAGE_HEIGHT") or 240)
AI_MODEL_URL = os.getenv("AI_MODEL_URL") or 'undefined'
NORMALIZE = True

if os.getenv("NORMALIZE"):
    if os.getenv("NORMALIZE").lower() == 'false':
        NORMALIZE = False

app = Flask(__name__)
CORS(app)

session = requests.Session()
session.trust_env = False

normalize = True
if os.getenv("NORMALIZE"):
    if os.getenv("NORMALIZE").lower() == 'false':
        normalize = False

def env_var_override(request):

    global IMAGE_WIDTH
    global IMAGE_HEIGHT
    global AI_MODEL_URL
    global NORMALIZE

    # Parsing form data
    fields = dict(request.form)

    # Override parameters with form data fields
    if 'width' in fields:
        print('IMAGE_WIDTH overridden')
        IMAGE_WIDTH = fields['width']

    if 'height' in fields:
        print('IMAGE_HEIGHT overridden')
        IMAGE_HEIGHT = fields['height']

    if 'ai_url' in fields:
        print('AI_MODEL_URL overridden')
        AI_MODEL_URL = fields['ai_url']

    if 'normalize' in fields:
        if fields['normalize'].lower() == 'false':
            print('NORMALIZE overridden')
            NORMALIZE = False

def preprocess_image(image):

    # Resizing
    if IMAGE_WIDTH and IMAGE_HEIGHT:
        image = cv2.resize(
            image,
            dsize=(int(IMAGE_WIDTH), int(IMAGE_HEIGHT)),
            interpolation=cv2.INTER_CUBIC )

    # Normnalization
    if NORMALIZE:
        image = image/255.00

    return image


@app.route('/', methods=['GET'])
def home():

    return json.dumps( {
    'applicationname': 'Image to JSONified array',
    'author': 'Maxime MOREILLON',
    'version': '1.2.2',
    } )

@app.route('/predict', methods=['POST'])
def predict():

    # Check if the request contains files
    if not request.files:
        print('Files not found in request body')
        return 'Files not found in request body', 400


    env_var_override(request)

    # Initialize an empty image list
    image_list = []

    for key, file in request.files.items():
        image = cv2.imdecode(np.fromstring(file.read(), np.uint8), cv2.IMREAD_UNCHANGED)
        image = preprocess_image(image)
        image_list.append(image.tolist())

    # Send data to the AI
    payload =  json.dumps({'instances': image_list })
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

    try:
        # Might be betyter with json=payload
        response = session.post(AI_MODEL_URL, data=payload, headers=headers)

    except Exception as e:
        print('Failed to communicate with {}'.format(AI_MODEL_URL))
        return 'Failed to communicate with the AI', 500


    if response.status_code ==  200:
        return response.json()
    else:
        print('Request failed with code {}'.format(response.status_code))
        return 'Request failed with code {}'.format(response.status_code), 400


if __name__ == '__main__':
    app.run('0.0.0.0',7436)
